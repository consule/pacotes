<?php

namespace Ow\Manageable\Http;

use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Ow\Manageable\Contracts\Manageable;
use Ow\Manageable\Entities\RepositoryFactory;
use Ow\Manageable\Entities\EntityFactory;
use Ow\Manageable\Events\MediaAttachedTo;
use Illuminate\Support\Facades\Validator;
use Image;
use Storage;

class MediaController extends Controller
{
    public function store(Request $request)
    {
        $entity = resolve(config('manageable.mediahub_model'));

        $media = $this->processMedia($entity, $request);

        if (!$request->has('title')) {
            $title_name = str_replace(
                '.'.$request->file('file')->getClientOriginalExtension(),
                '',
                $request->file('file')->getClientOriginalName()
            );

            $title_name = ucwords(str_replace('-', ' ', $title_name));

            $media->setCustomProperty('title', $title_name);

            $media->save();
        }

        return $this->respond($this->parseMedia($media));
    }

    public function attach($entity_name, $entity_id, Request $request)
    {
        $entity = EntityFactory::build($entity_name);

        if ($entity === null || !$entity instanceof Manageable) {
            return $this->respondNotFound();
        }

        $repository = RepositoryFactory::build($entity);
        $entity = $repository->pushCriteria(new Criteria($request))
            ->findWithoutFail($entity_id);

        if ($entity === null) {
            return $this->respondNotFound();
        }

        $this->checkPolicies($entity, 'media-attach');

        $media = $this->processMedia($entity, $request);

        event(new MediaAttachedTo($entity, $media));

        return $this->respond($this->parseMedia($media));
    }

    public function destroy($entity_name, $entity_id, $media_id, Request $request)
    {
        $entity = EntityFactory::build($entity_name);

        if ($entity === null || !$entity instanceof Manageable) {
            return $this->respondNotFound();
        }

        $repository = RepositoryFactory::build($entity);
        $entity = $repository->pushCriteria(new Criteria($request))
            ->findWithoutFail($entity_id);

        if ($entity === null) {
            return $this->respondNotFound();
        }

        $this->checkPolicies($entity, 'media-destroy');

        $media = $entity->media()->find($media_id);

        if ($media === null) {
            return $this->respondNotFound();
        }

        $media->delete();

        return $this->respondAccepted();
    }

    public function updateMedia($media_id, Request $request)
    {
        $entity = EntityFactory::build('system/medias');

        if ($entity === null) {
            return $this->respondNotFound();
        }

        $entity = $entity->where('id', $media_id)->first();

        if ($entity === null) {
            return $this->respondNotFound();
        }

        if ($request->has('collection')) {
            $entity->collection_name = $request->input('collection');
        }

        if ($request->has('title')) {
            $entity->setCustomProperty('title', $request->input('title'));
        }

        if ($request->has('description')) {
            $entity->setCustomProperty('description', $request->input('description'));
        }

        $entity->save();

        return !empty($entity) ? $this->respond($this->parseMedia($entity)) : $this->respondAccepted();
    }

    public function update($entity_name, $entity_id, $media_id, Request $request)
    {
        if ($request->has('file')) {
            $this->destroy($entity_name, $entity_id, $media_id, $request);
        }

        $entity = EntityFactory::build($entity_name);

        if ($entity === null || !$entity instanceof Manageable) {
            return $this->respondNotFound();
        }

        $repository = RepositoryFactory::build($entity);
        $entity = $repository->pushCriteria(new Criteria($request))
            ->findWithoutFail($entity_id);

        if ($entity === null) {
            return $this->respondNotFound();
        }

        if ($request->has('file')) {
            $media = $this->processMedia($entity, $request);

            $media_id = $media->id;
        }

        $media = $entity->media()->find($media_id);

        if ($media === null) {
            return $this->respondNotFound();
        }

        if ($request->has('collection')) {
            $media->collection_name = $request->input('collection');
        }

        if ($request->has('title')) {
            $media->setCustomProperty('title', $request->input('title'));
        }

        if ($request->has('description')) {
            $media->setCustomProperty('description', $request->input('description'));
        }

        $media->save();

        return !empty($media) ? $this->respond($this->parseMedia($media)) : $this->respondAccepted();
    }

    protected function parseMedia($media)
    {
        $parsed_media = array_merge([
            '_links' => [
                'thumb' => $media->hasEdited() ? $media->getFirstMediaUrl('edited') : $media->getUrl(),
                'original' => $media->getUrl(),
            ],
        ], $media->toArray());

        // check the type of the midia to put the image atributes in array
        try {
            if ($this->mediaTypeCheck($media, 'image')) {
                list($width, $height) = getimagesize($media->getPath());
                $parsed_media = array_merge($parsed_media, ['image' => ['width' => $width, 'height' => $height]]);
            }
        } catch (\Exception $e) {
            $this->logOrThrow($e);
        }

        return $parsed_media;
    }

    protected function mediaTypeCheck($media, string $mine_type): bool
    {
        return strpos($media->mime_type, $mine_type) !== false;
    }

    public function compressMedia($path)
    {
        $size = Image::make($path);
        $size = $size->filesize();

        if ($size >= 500000) {
            try {
                $info = getimagesize($path);

                if ($info['mime'] == 'image/jpeg') {
                    $image = imagecreatefromjpeg($path);
                } elseif ($info['mime'] == 'image/gif') {
                    $image = imagecreatefromgif($path);
                } elseif ($info['mime'] == 'image/png') {
                    $image = imagecreatefrompng($path);
                }

                imagejpeg($image, $path, 99);
            } catch (\Exception $e) {
                $img = Image::make($path);
                $img->encode('jpg', 99);
                $img->save($path);
            }
        }
    }

    public function resizeMedia($path, $width, $height)
    {
        $img = Image::make($path);
        $img->resize($width, $height);
        $img->save($path);
    }

    public function interlaceMedia($path)
    {
        $size = Image::make($path);
        $size = $size->filesize();

        if ($size >= 500000) {
            $img = Image::make($path);
            $img->interlace();
            $img->save($path);
        }
    }

    /**
     * @todo Create a specifc request for the media?
     */
    protected function processMedia(HasMedia $entity, Request $request)
    {
        $custom_properties = [];
        $collection = $request->input('collection', 'default');

        Validator::make($request->all(), $entity::getMediaRules($collection))->validate();

        $file = $request->file('file');

        $extension = $file->getClientOriginalExtension();
        // $filename = date('Y-m-d-h-i-s').'_'.sha1($file->getClientOriginalName()).'.'.$extension;
        $filename = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'.'.$extension;

        if (in_array($file->getClientOriginalExtension(), array('jpeg', 'png', 'jpg', 'gif', 'JPEG', 'PNG', 'JPG', 'GIF'))) {
            $this->compressMedia($file->getPathName());

            if ($request->has('width') && $request->has('height')) {
                $this->resizeMedia($file->getPathName(), $request->input('width'), $request->input('height'));
            }

            $this->interlaceMedia($file->getPathName());
        }

        if (in_array($file->getClientOriginalExtension(), array('jpeg', 'png', 'jpg', 'gif', 'svg', 'JPEG', 'PNG', 'JPG', 'GIF', 'SVG')) && $request->has('base64')) {
            $custom_properties['base64'] = base64_encode(file_get_contents($file->getPathName()));
        }

        $file = $file->move(config('manageable.temp_dir'), $filename);

        $media = $entity->addMedia($file);

        if ($request->has('title')) {
            $custom_properties['title'] = $request->input('title');
        }

        if ($request->has('description')) {
            $custom_properties['description'] = $request->input('description');
        }

        $media = $media->withCustomProperties($custom_properties);

        $media = $media->toMediaCollection($collection);

        return $media;
    }

    public function download($entity_name, $entity_id, $media_id, Request $request)
    {
        $entity = EntityFactory::build($entity_name);

        if ($entity === null || !$entity instanceof Manageable) {
            return $this->respondNotFound();
        }

        $repository = RepositoryFactory::build($entity);
        $entity = $repository->pushCriteria(new Criteria($request))
            ->findWithoutFail($entity_id);

        if ($entity === null) {
            return $this->respondNotFound();
        }

        $media_entry = $entity->media()->find($media_id);

        if (empty($media_entry)) {
            return $this->respondNotFound();
        }

        $fs = Storage::disk($media_entry->disk)->getDriver();
        $file_name = "/{$media_id}/{$media_entry->file_name}";
        $stream = $fs->readStream($file_name);

        return response()->stream(function () use ($stream) {
            fpassthru($stream);
        }, 200, [
            'Content-Type' => $fs->getMimetype($file_name),
            'Content-Length' => $fs->getSize($file_name),
            'Content-disposition' => 'attachment; filename="'.$media_entry->file_name.'"',
        ]);
    }
}
